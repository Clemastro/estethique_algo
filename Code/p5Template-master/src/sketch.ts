// -------------------
//  Parameters and UI
// -------------------

const gui = new dat.GUI()
const params = {
    Taille: 100,
    difference: 0.75,
    angle: 30,
    maxligne: 8,
    hauteur: 500,
    Download_Image: () => save(),
}

gui.add(params, "Download_Image")
gui.add(params, "Taille", 0, 200, 1)
gui.add(params, "difference", 0, 2, 0.01)
gui.add(params, "angle", 0, 360, 1)
gui.add(params, "maxligne", 0, 15, 1)
gui.add(params, "hauteur", 0, 1000, 1)

// -------------------
//       Drawing
// -------------------
var x = 0;
var y = 0;
var speed = 1;
function draw() {
    //set angle mode at degree
    angleMode(DEGREES)

    //fix a random seed
    randomSeed(0)

    //create a black background
    background("white")

    //you can change the mode of the program by press button 1, 2 of the keybord
    fill(50)
    textSize(20)
    text("press 1 to show the classic program", 50, 100)
    text("press 2 to show the extended (interactive) program", 50, 200)
    text("press 3 to show the animate program", 50, 300)

    if (key == '1') {
        background(0);   //erase canvas
        //create the first line
        stroke("white") //white color
        translate(width/2, params.hauteur) //origin position
        line(0, 0, 0, -params.Taille) //place points of the line
        ligne(-params.Taille, params.maxligne) //call recursiv function for fractals (classic version)
    }
    if (key == '2') {
        background(0);   //erase canvas
        //create the first line
        stroke("white") //white color
        translate(width/2, params.hauteur) //origin position
        line(0, 0, 0, -params.Taille) //place points of the line
        ligneV2(-params.Taille, params.maxligne) //call recursiv function for fractals (extended version)
    }
    if (key == '3') {
        background(0);   //erase canvas
        //create the first line
        stroke("white") //white color
        translate(width/2, params.hauteur) //origin position
        line(0, 0, 0, -params.Taille) //place points of the line
        ligneV3(-params.Taille, params.maxligne, x, y) //call recursiv function for fractals (extended version)
    }
    x+=speed; //each loop, x value update with speed to create an animation for ligneV3
    y+=speed; //each loop, y value update with speed to create an animation for ligneV3
}

function ligne(taille, nbligne){
    if(nbligne>0){ //recursiv condition stop
        translate(0, taille) //place origin to the last point of the line
        push()
            rotate(params.angle) //rotation of the line
            line(0, 0, 0, taille*params.difference)
            ligne(taille*params.difference, nbligne-1)
        pop()
        push()
            rotate(-params.angle)
            line(0, 0, 0, taille*params.difference)
            ligne(taille*params.difference, nbligne-1)
        pop()
    }
}

function ligneV2(taille, nbligne){

    //Add colors
        /*let b = 0
        let r
        let g
        if(nbligne>4){
            r = 110
            g = 100-(nbligne*10)
        }
        else{
            r = nbligne*18
            g = 110
        }    
        stroke(r, g, b)*/

    if(nbligne>0){ //recursiv condition stop
        translate(0, taille) //place origin to the last point of the line
        push()
            rotate(params.angle+((height/2)+mouseX/8)+mouseY/8) //rotation of the line
            line(0, 0, 0, taille*params.difference)
            ligneV2(taille*params.difference, nbligne-1)
        pop()
        push()
            rotate(-params.angle+((height/2)+mouseX/8)-mouseY/8)
            line(0, 0, 0, taille*params.difference)
            ligneV2(taille*params.difference, nbligne-1)
        pop()
    }
}

function ligneV3(taille, nbligne, x, y){
    if(nbligne>0){ //recursiv condition stop
        translate(0, taille) //place origin to the last point of the line
        push()
            rotate(params.angle+((height/2)+x/8)+y/8) //rotation of the line
            line(0, 0, 0, taille*params.difference)
            ligneV3(taille*params.difference, nbligne-1, x, y)
        pop()
        push()
            rotate(-params.angle+((height/2)-x/8)-y/8)
            line(0, 0, 0, taille*params.difference)
            ligneV3(taille*params.difference, nbligne-1, -x, -y)
        pop()
    }
}

// -------------------
//    Initialization
// -------------------

function setup() {
    p6_CreateCanvas()
}

function windowResized() {
    p6_ResizeCanvas()
}