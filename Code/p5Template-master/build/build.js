var gui = new dat.GUI();
var params = {
    Taille: 100,
    difference: 0.75,
    angle: 30,
    maxligne: 8,
    hauteur: 500,
    Download_Image: function () { return save(); },
};
gui.add(params, "Download_Image");
gui.add(params, "Taille", 0, 200, 1);
gui.add(params, "difference", 0, 2, 0.01);
gui.add(params, "angle", 0, 360, 1);
gui.add(params, "maxligne", 0, 15, 1);
gui.add(params, "hauteur", 0, 1000, 1);
var x = 0;
var y = 0;
var speed = 1;
function draw() {
    angleMode(DEGREES);
    randomSeed(0);
    background("white");
    fill(50);
    textSize(20);
    text("press 1 to show the classic program", 50, 100);
    text("press 2 to show the extended (interactive) program", 50, 200);
    text("press 3 to show the animate program", 50, 300);
    if (key == '1') {
        background(0);
        stroke("white");
        translate(width / 2, params.hauteur);
        line(0, 0, 0, -params.Taille);
        ligne(-params.Taille, params.maxligne);
    }
    if (key == '2') {
        background(0);
        stroke("white");
        translate(width / 2, params.hauteur);
        line(0, 0, 0, -params.Taille);
        ligneV2(-params.Taille, params.maxligne);
    }
    if (key == '3') {
        background(0);
        stroke("white");
        translate(width / 2, params.hauteur);
        line(0, 0, 0, -params.Taille);
        ligneV3(-params.Taille, params.maxligne, x, y);
    }
    x += speed;
    y += speed;
}
function ligne(taille, nbligne) {
    if (nbligne > 0) {
        translate(0, taille);
        push();
        rotate(params.angle);
        line(0, 0, 0, taille * params.difference);
        ligne(taille * params.difference, nbligne - 1);
        pop();
        push();
        rotate(-params.angle);
        line(0, 0, 0, taille * params.difference);
        ligne(taille * params.difference, nbligne - 1);
        pop();
    }
}
function ligneV2(taille, nbligne) {
    if (nbligne > 0) {
        translate(0, taille);
        push();
        rotate(params.angle + ((height / 2) + mouseX / 8) + mouseY / 8);
        line(0, 0, 0, taille * params.difference);
        ligneV2(taille * params.difference, nbligne - 1);
        pop();
        push();
        rotate(-params.angle + ((height / 2) + mouseX / 8) - mouseY / 8);
        line(0, 0, 0, taille * params.difference);
        ligneV2(taille * params.difference, nbligne - 1);
        pop();
    }
}
function ligneV3(taille, nbligne, x, y) {
    if (nbligne > 0) {
        translate(0, taille);
        push();
        rotate(params.angle + ((height / 2) + x / 8) + y / 8);
        line(0, 0, 0, taille * params.difference);
        ligneV3(taille * params.difference, nbligne - 1, x, y);
        pop();
        push();
        rotate(-params.angle + ((height / 2) - x / 8) - y / 8);
        line(0, 0, 0, taille * params.difference);
        ligneV3(taille * params.difference, nbligne - 1, -x, -y);
        pop();
    }
}
function setup() {
    p6_CreateCanvas();
}
function windowResized() {
    p6_ResizeCanvas();
}
var __ASPECT_RATIO = 1;
var __MARGIN_SIZE = 25;
function __desiredCanvasWidth() {
    var windowRatio = windowWidth / windowHeight;
    if (__ASPECT_RATIO > windowRatio) {
        return windowWidth - __MARGIN_SIZE * 2;
    }
    else {
        return __desiredCanvasHeight() * __ASPECT_RATIO;
    }
}
function __desiredCanvasHeight() {
    var windowRatio = windowWidth / windowHeight;
    if (__ASPECT_RATIO > windowRatio) {
        return __desiredCanvasWidth() / __ASPECT_RATIO;
    }
    else {
        return windowHeight - __MARGIN_SIZE * 2;
    }
}
var __canvas;
function __centerCanvas() {
    __canvas.position((windowWidth - width) / 2, (windowHeight - height) / 2);
}
function p6_CreateCanvas() {
    __canvas = createCanvas(__desiredCanvasWidth(), __desiredCanvasHeight());
    __centerCanvas();
}
function p6_ResizeCanvas() {
    resizeCanvas(__desiredCanvasWidth(), __desiredCanvasHeight());
    __centerCanvas();
}
var p6_SaveImageSequence = function (durationInFrames, fileExtension) {
    if (frameCount <= durationInFrames) {
        noLoop();
        var filename_1 = nf(frameCount - 1, ceil(log(durationInFrames) / log(10)));
        var mimeType = (function () {
            switch (fileExtension) {
                case 'png':
                    return 'image/png';
                case 'jpeg':
                case 'jpg':
                    return 'image/jpeg';
            }
        })();
        __canvas.elt.toBlob(function (blob) {
            p5.prototype.downloadFile(blob, filename_1, fileExtension);
            setTimeout(function () { return loop(); }, 100);
        }, mimeType);
    }
};
//# sourceMappingURL=../src/src/build.js.map